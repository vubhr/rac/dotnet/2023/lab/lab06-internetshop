using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using InternetShop.Models;

    public class InternetShopDbContext : DbContext
    {
        public InternetShopDbContext (DbContextOptions<InternetShopDbContext> options)
            : base(options)
        {
        }

        public DbSet<InternetShop.Models.Product> Products { get; set; } = default!;
    }
