namespace InternetShop.Models;

public class ProductService : IProductService
{
    private readonly InternetShopDbContext _context;

    public ProductService(InternetShopDbContext context)
    {
        _context = context;
    }

    public async Task<IEnumerable<Product>> GetProductsAsync()
    {
        return await _context.Products.ToListAsync();
    }

    public async Task<Product> GetProductAsync(int id)
    {
        return await _context.Products.FirstOrDefaultAsync(m => m.Id == id);
    }

    public async Task<Product> CreateProductAsync(Product product)
    {
        _context.Add(product);
        await _context.SaveChangesAsync();
        return product;
    }

    public async Task<Product> UpdateProductAsync(Product product)
    {
        _context.Update(product);
        await _context.SaveChangesAsync();
        return product;
    }

    public async Task<Product> DeleteProductAsync(int id)
    {
        var product = await _context.Products.FindAsync(id);
        _context.Products.Remove(product);
        await _context.SaveChangesAsync();
        return product;
    }

    public bool ProductExists(int id)
    {
        return _context.Products.Any(e => e.Id == id);
    }
}
